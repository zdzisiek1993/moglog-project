package com.example.MogLogProject.enums;

public enum UserRole {
    ADMINISTRATOR, MODERATOR, VERIFIER, USER, GUEST;
}
