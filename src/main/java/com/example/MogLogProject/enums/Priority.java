package com.example.MogLogProject.enums;

public enum Priority {
    NORMAL, URGENT;
}
