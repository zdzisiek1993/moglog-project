package com.example.MogLogProject.model;

import com.example.MogLogProject.enums.UserRole;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.OffsetDateTime;
import java.util.List;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
@Entity
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @OneToMany(mappedBy = "user")
    private List<Agreement> agreementList;

    @OneToMany(mappedBy = "user")
    private List<Comment> commentList;

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
//    @JoinColumn(name = "role_id")
    private Role role;

    private String login;
    private String firstName;
    private String lastName;
    private String password;
    private UserRole userRole;
    private String email;
    private OffsetDateTime createdAt;

}
