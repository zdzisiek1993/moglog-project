package com.example.MogLogProject.model;

import com.example.MogLogProject.enums.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.OffsetDateTime;
import java.util.List;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
@Entity
public class Agreement {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @OneToMany(mappedBy = "agreement")
    private List<Comment> commentList;

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
//    @JoinColumn(name = "user_id")
    private User user;

    private Status status;
    private Priority priority;
    private String procedureNumber;
    private String identifier;
    private String subject;
    private String nipNumber;
    private String subjectOfTheContract;
    private Double contractValue;
    private OffsetDateTime createdAt;
    private Annex annex;
    private Mode mode;
    private String unit;
    private RODO rodo;
    private String applicantUnit;
    private String advisoryBody;
}
