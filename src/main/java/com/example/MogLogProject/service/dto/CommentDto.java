package com.example.MogLogProject.service.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.OffsetDateTime;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
public class CommentDto {
    private Integer id;
    private String text;
    private OffsetDateTime createdAt;
    private OffsetDateTime updateAt;
}
