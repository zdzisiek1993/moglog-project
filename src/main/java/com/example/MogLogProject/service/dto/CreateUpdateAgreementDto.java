package com.example.MogLogProject.service.dto;

import com.example.MogLogProject.enums.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
public class CreateUpdateAgreementDto {
    private Integer id;
    private Status status;
    private Priority priority;
    private String procedureNumber;
    private String identifier;
    private String subject;
    private String nipNumber;
    private String subjectOfTheContract;
    private Double contractValue;
    private Annex annex;
    private Mode mode;
    private String unit;
    private RODO rodo;
    private String applicantUnit;
    private String advisoryBody;
}
