package com.example.MogLogProject.service.dto;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
public class CreateUpdateCommentDto {
    private Integer agreementId; // 9
    private Integer userId; // 4
    private String text;
}
