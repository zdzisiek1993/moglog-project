package com.example.MogLogProject.service.mapper;

import com.example.MogLogProject.model.Role;
import com.example.MogLogProject.service.dto.CreateUpdateRoleDto;
import com.example.MogLogProject.service.dto.RoleDto;
import org.springframework.stereotype.Service;

@Service
public class RoleDtoMapper {
    private static int idCounter = 0;
    public RoleDto toDto(Role role){
        return RoleDto.builder()
                .id(role.getId())
                .name(role.getName())
                .createdAt((role.getCreatedAt()))
                .build();
    }

    public Role toModel(CreateUpdateRoleDto newRole){
        return Role.builder()
                .id(newRole.getId())
                .name(newRole.getName())
                .build();
    }
}
