package com.example.MogLogProject.service.mapper;

import com.example.MogLogProject.model.Agreement;
import com.example.MogLogProject.service.dto.AgreementDto;
import com.example.MogLogProject.service.dto.CreateUpdateAgreementDto;
import org.springframework.stereotype.Service;

@Service
public class AgreementDtoMapper {
    private static int idCounter = 0;

    public AgreementDto toDto (Agreement agreement) {
        return AgreementDto.builder()
                .id(agreement.getId())
                .status(agreement.getStatus())
                .priority(agreement.getPriority())
                .procedureNumber(agreement.getProcedureNumber())
                .identifier(agreement.getIdentifier())
                .subject(agreement.getSubject())
                .nipNumber(agreement.getNipNumber())
                .subjectOfTheContract(agreement.getSubjectOfTheContract())
                .contractValue(agreement.getContractValue())
                .createdAt(agreement.getCreatedAt())
                .annex(agreement.getAnnex())
                .mode(agreement.getMode())
                .unit(agreement.getUnit())
                .rodo(agreement.getRodo())
                .applicantUnit(agreement.getApplicantUnit())
                .advisoryBody(agreement.getAdvisoryBody())
                .build();
    }
    public Agreement toModel (CreateUpdateAgreementDto newAgreement) {
        return Agreement.builder()
                .id(newAgreement.getId())
                .status(newAgreement.getStatus())
                .priority(newAgreement.getPriority())
                .procedureNumber(newAgreement.getProcedureNumber())
                .identifier(newAgreement.getIdentifier())
                .subject(newAgreement.getSubject())
                .nipNumber(newAgreement.getNipNumber())
                .subjectOfTheContract(newAgreement.getSubjectOfTheContract())
                .contractValue(newAgreement.getContractValue())
                .annex(newAgreement.getAnnex())
                .mode(newAgreement.getMode())
                .unit(newAgreement.getUnit())
                .rodo(newAgreement.getRodo())
                .applicantUnit(newAgreement.getApplicantUnit())
                .advisoryBody(newAgreement.getAdvisoryBody())
                .build();
    }
}
