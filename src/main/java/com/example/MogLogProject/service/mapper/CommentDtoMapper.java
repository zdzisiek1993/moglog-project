package com.example.MogLogProject.service.mapper;

import com.example.MogLogProject.model.Comment;
import com.example.MogLogProject.service.dto.CommentDto;
import com.example.MogLogProject.service.dto.CreateUpdateCommentDto;
import org.springframework.stereotype.Service;

@Service
public class CommentDtoMapper {
    private static int idCounter = 0;
    public CommentDto toDto(Comment comment){
        return CommentDto.builder()
                .id(comment.getId())
                .text(comment.getText())
                .createdAt(comment.getCreatedAt())
                .updateAt(comment.getUpdateAt())
                .build();
    }
    public Comment toModel(CreateUpdateCommentDto newComment){
        return Comment.builder()
                .id(newComment.getUserId())
                .text(newComment.getText())
                .build();
    }
}
