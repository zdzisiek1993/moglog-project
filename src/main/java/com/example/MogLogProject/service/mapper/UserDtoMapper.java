package com.example.MogLogProject.service.mapper;

import com.example.MogLogProject.model.User;
import com.example.MogLogProject.service.dto.CreateUpdateUserDto;
import com.example.MogLogProject.service.dto.UserDTO;
import org.springframework.stereotype.Service;

@Service
public class UserDtoMapper {
    private static int idCounter = 0;

    public UserDTO toDto(User user){
        return UserDTO.builder()
                .id(user.getId())
                .login(user.getLogin())
                .firstName(user.getFirstName())
                .lastName(user.getLastName())
                .email(user.getEmail())
                .createdAt(user.getCreatedAt())
                .build();
    }
    public User toModel(CreateUpdateUserDto newUser){
        return User.builder()
                .id(newUser.getId())
                .login(newUser.getLogin())
                .firstName(newUser.getFirstName())
                .lastName(newUser.getLastName())
                .email(newUser.getEmail())
                .build();
    }

}
