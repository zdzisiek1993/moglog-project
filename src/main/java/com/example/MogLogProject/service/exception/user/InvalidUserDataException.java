package com.example.MogLogProject.service.exception.user;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class InvalidUserDataException extends Exception {
    public InvalidUserDataException(String message){
        super(message);
    }
}
