package com.example.MogLogProject.service.exception.comment;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class InvalidCommentDataException extends Exception {
    public InvalidCommentDataException(String message){
        super(message);
    }
}
