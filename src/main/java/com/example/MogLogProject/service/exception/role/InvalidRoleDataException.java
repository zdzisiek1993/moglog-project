package com.example.MogLogProject.service.exception.role;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class InvalidRoleDataException extends Exception {
    public InvalidRoleDataException(String message) {
        super(message);
    }
}
