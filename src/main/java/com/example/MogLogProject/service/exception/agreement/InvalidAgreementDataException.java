package com.example.MogLogProject.service.exception.agreement;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class InvalidAgreementDataException extends Exception {
    public InvalidAgreementDataException(String message) {
        super(message);
    }

}
