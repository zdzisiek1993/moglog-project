package com.example.MogLogProject.service.services;

import com.example.MogLogProject.model.Agreement;
import com.example.MogLogProject.model.Comment;
import com.example.MogLogProject.model.User;
import com.example.MogLogProject.repository.AgreementRepository;
import com.example.MogLogProject.repository.CommentRepository;
import com.example.MogLogProject.repository.UserRepository;
import com.example.MogLogProject.service.dto.CommentDto;
import com.example.MogLogProject.service.dto.CreateUpdateCommentDto;
import com.example.MogLogProject.service.exception.agreement.AgreementNotFoundException;
import com.example.MogLogProject.service.exception.comment.CommentNotFoundException;
import com.example.MogLogProject.service.exception.comment.InvalidCommentDataException;
import com.example.MogLogProject.service.exception.user.UserNotFoundException;
import com.example.MogLogProject.service.mapper.CommentDtoMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class CommentService {

    @Autowired
    private CommentDtoMapper mapper;
    @Autowired
    private CommentRepository commentRepository;
    @Autowired
    private AgreementRepository agreementRepository;
    @Autowired
    private UserRepository userRepository;

    private static Integer idCounter = 0;
    private List<Comment> comments = new ArrayList<>();

    @Transactional
    public List<CommentDto> getAllComments() {
        List<CommentDto> dtos = new ArrayList<>();
        List<Comment> allComments = commentRepository.findAll();
        for( Comment comment : allComments) {
            CommentDto dto = mapper.toDto(comment);
            dtos.add(dto);
        }
        return dtos;
    }

    @Transactional
    public CommentDto getCommentById(int id) throws CommentNotFoundException{
        Optional<Comment> optionalComment = commentRepository.findById(id);
        if(optionalComment.isPresent()) {
            CommentDto dto = mapper.toDto(optionalComment.get());
            return dto;
        }else {
            throw new CommentNotFoundException("Comment not found!");
        }
    }
    @Transactional
    public CommentDto createNewComment(CreateUpdateCommentDto dto) throws InvalidCommentDataException, AgreementNotFoundException, UserNotFoundException {
        if(dto.getText() == null || dto.getText().length() < 5)  {
            throw new InvalidCommentDataException("Comment data is invalid!");
        }
        Comment comment = mapper.toModel(dto);

        Integer agreementId = dto.getAgreementId();
        Agreement agreement = agreementRepository.findById(agreementId)
                .orElseThrow(() -> new AgreementNotFoundException("Agreement not found"));

        Integer userId = dto.getUserId();
        User user = userRepository.findById(userId).orElseThrow(() -> new UserNotFoundException("User not found"));

        comment.setAgreement(agreement);
        comment.setUser(user);

        commentRepository.save(comment);

        return mapper.toDto(comment);
    }

    @Transactional
    public CommentDto updateCommentById(int id, CreateUpdateCommentDto dto) throws
            CommentNotFoundException, InvalidCommentDataException{
        if(dto.getText() == null || dto.getText().length() < 5)  {
            throw new InvalidCommentDataException("Comment data is invalid!");
        }
        Comment commentToUpdate = commentRepository.findById(id)
                .orElseThrow(() -> new CommentNotFoundException("Comment not found!"));

        commentToUpdate.setId(dto.getUserId());
        commentToUpdate.setText(dto.getText());
        commentToUpdate.setCreatedAt(OffsetDateTime.now());
        commentToUpdate.setUpdateAt(OffsetDateTime.now());

        Comment saveComment = commentRepository.save(commentToUpdate);
        return mapper.toDto(saveComment);
    }

    @Transactional
    public CommentDto deleteCommentById( int id) throws CommentNotFoundException{
        Comment deletedComment = commentRepository.findById(id)
                .orElseThrow(() -> new CommentNotFoundException("Comment not found!"));
        commentRepository.delete(deletedComment);

        return mapper.toDto(deletedComment);
    }

}
