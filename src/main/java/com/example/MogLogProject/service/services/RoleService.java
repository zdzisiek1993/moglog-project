package com.example.MogLogProject.service.services;

import com.example.MogLogProject.model.Role;
import com.example.MogLogProject.repository.RoleRepository;
import com.example.MogLogProject.service.dto.CreateUpdateRoleDto;
import com.example.MogLogProject.service.dto.RoleDto;
import com.example.MogLogProject.service.exception.role.InvalidRoleDataException;
import com.example.MogLogProject.service.exception.role.RoleNotFoundException;
import com.example.MogLogProject.service.mapper.RoleDtoMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class RoleService {

    @Autowired
    private RoleDtoMapper mapper;
    @Autowired
    private RoleRepository roleRepository;

    private static Integer idCounter = 0;
    private List<Role> roles = new ArrayList<>();

    @Transactional
    public List<RoleDto> getAllRoles() {
        List<RoleDto> dtos = new ArrayList<>();
        List<Role> allRoles = roleRepository.findAll();
        for (Role role : allRoles) {
            RoleDto dto = mapper.toDto(role);
            dtos.add(dto);
        }
        return dtos;
    }

    @Transactional
    public RoleDto getRoleById(int id) throws RoleNotFoundException {
        Optional<Role> optionalRole = roleRepository.findById(id);
        if(optionalRole.isPresent()) {
            RoleDto dto = mapper.toDto(optionalRole.get());
            return dto;
        }else {
            throw new RoleNotFoundException("Below Role do not exist!");
        }
    }

    @Transactional
    public RoleDto createNewRole(CreateUpdateRoleDto dto) throws InvalidRoleDataException {
        if(dto.getName() == null || dto.getName().length() < 3 ) {
            throw new InvalidRoleDataException("Invalid data");
        }
        Role role = mapper.toModel(dto);
        roleRepository.save(role);

        return mapper.toDto(role);
    }

    @Transactional
    public RoleDto updateRoleById(int id, CreateUpdateRoleDto dto) throws
            RoleNotFoundException, InvalidRoleDataException {
        if(dto.getName() == null || dto.getName().length() < 3) {
            throw new InvalidRoleDataException("Invalid data");
        }
        Role roleToUpdate = roleRepository.findById(id)
                .orElseThrow(() -> new RoleNotFoundException("Below Role do not exist!"));

        roleToUpdate.setId(dto.getId());
        roleToUpdate.setName(dto.getName());
        roleToUpdate.setCreatedAt(OffsetDateTime.now());

        Role saveRole = roleRepository.save(roleToUpdate);
        return mapper.toDto(saveRole);
    }

    @Transactional
    public RoleDto deleteRoleById(int id) throws RoleNotFoundException {
        Role deletedRole = roleRepository.findById(id)
                .orElseThrow(() -> new RoleNotFoundException("Below Role do not exist!"));
        roleRepository.delete(deletedRole);

        return mapper.toDto(deletedRole);
    }
}
