package com.example.MogLogProject.service.services;

import com.example.MogLogProject.model.Agreement;
import com.example.MogLogProject.repository.AgreementRepository;
import com.example.MogLogProject.service.dto.AgreementDto;
import com.example.MogLogProject.service.dto.CreateUpdateAgreementDto;
import com.example.MogLogProject.service.exception.agreement.AgreementNotFoundException;
import com.example.MogLogProject.service.exception.agreement.InvalidAgreementDataException;
import org.springframework.beans.factory.annotation.Autowired;
import com.example.MogLogProject.service.mapper.AgreementDtoMapper;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class AgreementService {

    @Autowired
    private AgreementDtoMapper mapper;
    @Autowired
    private AgreementRepository agreementRepository;

//    private static Integer idCounter = 0;
//    private List<Agreement> agreements = new ArrayList<>();

    @Transactional
    public List<AgreementDto> getAllAgreements() {
        List<AgreementDto> dtos = new ArrayList<>();
        List<Agreement> allAgreement = agreementRepository.findAll();
        for(Agreement agreement : allAgreement) {
            AgreementDto dto = mapper.toDto(agreement);
            dtos.add(dto);
        }
        return dtos;
    }

    @Transactional
    public AgreementDto getAgreementById(int id) throws AgreementNotFoundException {
        Optional<Agreement> optionalAgreement = agreementRepository.findById(id);
        if(optionalAgreement.isPresent()) {
            AgreementDto dto = mapper.toDto(optionalAgreement.get());
            return dto;
        }else {
            throw new AgreementNotFoundException("Agreement not found!");
        }
    }

    @Transactional
    public AgreementDto createNewAgreement(CreateUpdateAgreementDto dto) throws InvalidAgreementDataException {
        if(dto.getSubject() == null || dto.getSubject().length() < 2 || dto.getNipNumber() == null) {
            throw new InvalidAgreementDataException("Agreement data is wrong!");
        }
        Agreement agreement = mapper.toModel(dto);
        agreementRepository.save(agreement);

        return mapper.toDto(agreement);
    }

    @Transactional
    public AgreementDto updateAgreementById(int id, CreateUpdateAgreementDto dto) throws
            AgreementNotFoundException, InvalidAgreementDataException {
        if (dto.getStatus() == null || dto.getPriority() == null || dto.getProcedureNumber() == null ||
        dto.getIdentifier() == null || dto.getSubject() == null || dto.getNipNumber() == null ||
        dto.getSubjectOfTheContract() == null || dto.getContractValue() == null || dto.getAnnex() == null ||
        dto.getMode() == null || dto.getUnit() == null || dto.getRodo() == null || dto.getApplicantUnit() == null ||
        dto.getAdvisoryBody() == null) {
            throw new InvalidAgreementDataException("Agreement data is wrong!");
        } //todo dopisac wszystkie zaleznosci przy podawaniu danych
        Agreement agreementToUpdate = agreementRepository.findById(id)
                .orElseThrow(() -> new AgreementNotFoundException("Agreement not found!"));

        agreementToUpdate.setId(dto.getId());
        agreementToUpdate.setStatus(dto.getStatus());
        agreementToUpdate.setPriority(dto.getPriority());
        agreementToUpdate.setProcedureNumber(dto.getProcedureNumber());
        agreementToUpdate.setIdentifier(dto.getIdentifier());
        agreementToUpdate.setSubject(dto.getSubject());
        agreementToUpdate.setNipNumber(dto.getNipNumber());
        agreementToUpdate.setSubjectOfTheContract(dto.getSubjectOfTheContract());
        agreementToUpdate.setContractValue(dto.getContractValue());
        agreementToUpdate.setCreatedAt(OffsetDateTime.now());
        agreementToUpdate.setAnnex(dto.getAnnex());
        agreementToUpdate.setMode(dto.getMode());
        agreementToUpdate.setUnit(dto.getUnit());
        agreementToUpdate.setRodo(dto.getRodo());
        agreementToUpdate.setApplicantUnit(dto.getApplicantUnit());
        agreementToUpdate.setAdvisoryBody(dto.getAdvisoryBody());

        Agreement saveAgreement = agreementRepository.save(agreementToUpdate);
        return mapper.toDto(saveAgreement);
    }

    @Transactional
    public AgreementDto deleteAgreementById(int id) throws AgreementNotFoundException {
        Agreement deletedAgreement = agreementRepository.findById(id)
                .orElseThrow(() -> new AgreementNotFoundException("Agreement not found"));
        agreementRepository.delete(deletedAgreement);

        return mapper.toDto(deletedAgreement);
    }

}
