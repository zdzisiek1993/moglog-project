package com.example.MogLogProject.service.services;

import com.example.MogLogProject.model.User;
import com.example.MogLogProject.repository.UserRepository;
import com.example.MogLogProject.service.dto.CreateUpdateUserDto;
import com.example.MogLogProject.service.dto.UserDTO;
import com.example.MogLogProject.service.exception.user.InvalidUserDataException;
import com.example.MogLogProject.service.exception.user.UserNotFoundException;
import com.example.MogLogProject.service.mapper.UserDtoMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class UserService {

    @Autowired
    private UserDtoMapper mapper;
    @Autowired
    private UserRepository userRepository;

//    private static Integer idCounter = 0;
//    private List<User> users = new ArrayList<>();

    @Transactional
    public List<UserDTO> getAllUsers() {
        List<UserDTO> dtos = new ArrayList<>();
        List<User> allUsers = userRepository.findAll();
        for(User user : allUsers) {
            UserDTO dto = mapper.toDto(user);
            dtos.add(dto);
        }
        return dtos;
    }

    @Transactional
    public UserDTO getUserById(int id) throws UserNotFoundException {
        Optional<User> optionalUser = userRepository.findById(id);
        if(optionalUser.isPresent()) {
            UserDTO dto = mapper.toDto(optionalUser.get());
            return dto;
        }else {
            throw new UserNotFoundException("User do not exist!");
    }
    }

    @Transactional
    public UserDTO createNewUser(CreateUpdateUserDto dto) throws InvalidUserDataException {
        if(dto.getLogin().length() < 5 || dto.getLogin() == null ||
            dto.getFirstName() == null || dto.getLastName() == null ||
            dto.getEmail() == null)  {
            throw new InvalidUserDataException("Wrong data input");
        }
        User user = mapper.toModel(dto);
        userRepository.save(user);

        return mapper.toDto(user);
    }

    @Transactional
    public UserDTO updateUserById(int id, CreateUpdateUserDto dto) throws
            UserNotFoundException, InvalidUserDataException {
        if(dto.getLogin().length() < 5 || dto.getLogin() == null ||
                dto.getFirstName() == null || dto.getLastName() == null ||
                dto.getEmail() == null) {
            throw new InvalidUserDataException("User data input is wrong");
        }
        User userToUpdate = userRepository.findById(id)
                .orElseThrow(() -> new UserNotFoundException("User not found!"));

        userToUpdate.setId(dto.getId());
        userToUpdate.setFirstName(dto.getFirstName());
        userToUpdate.setLastName(dto.getLastName());
        userToUpdate.setLogin(dto.getLogin());
        userToUpdate.setEmail(dto.getEmail());
        userToUpdate.setCreatedAt(OffsetDateTime.now());

        User saveUser = userRepository.save(userToUpdate);
        return mapper.toDto(saveUser);
    }

    @Transactional
    public UserDTO deleteUserById(int id) throws UserNotFoundException {
        User deletedUser = userRepository.findById(id)
                .orElseThrow(() -> new UserNotFoundException("User do not exist!"));
        userRepository.delete(deletedUser);

        return mapper.toDto(deletedUser);
    }

}
