package com.example.MogLogProject.controllers;

import com.example.MogLogProject.service.dto.CreateUpdateRoleDto;
import com.example.MogLogProject.service.dto.RoleDto;
import com.example.MogLogProject.service.exception.role.InvalidRoleDataException;
import com.example.MogLogProject.service.exception.role.RoleNotFoundException;
import com.example.MogLogProject.service.services.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/role")
public class RoleController {

    @Autowired
    private RoleService roleService;

    @GetMapping
    public List<RoleDto> getAllRoles() { return roleService.getAllRoles() ; }

    @GetMapping("/{id}")
    public  RoleDto getRoleById(@PathVariable int id) throws RoleNotFoundException {
        return roleService.getRoleById(id);
    }

    @PostMapping
    public RoleDto createNewRole(CreateUpdateRoleDto createUpdateRoleDto)
        throws InvalidRoleDataException {
        return roleService.createNewRole(createUpdateRoleDto);
    }

    @PutMapping("/{id}")
    public RoleDto updateRoleById (@PathVariable int id, CreateUpdateRoleDto createUpdateRoleDto)
        throws RoleNotFoundException, InvalidRoleDataException {
        return roleService.updateRoleById(id, createUpdateRoleDto);
    }

    @DeleteMapping("/{id}")
    public RoleDto deleteUserById(@PathVariable int id) throws RoleNotFoundException {
        return roleService.deleteRoleById(id);
    }

}
