package com.example.MogLogProject.controllers;

import com.example.MogLogProject.service.dto.CreateUpdateUserDto;
import com.example.MogLogProject.service.dto.UserDTO;
import com.example.MogLogProject.service.exception.user.InvalidUserDataException;
import com.example.MogLogProject.service.exception.user.UserNotFoundException;
import com.example.MogLogProject.service.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/user")
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping
    public List<UserDTO> getAllUsers() {
        return userService.getAllUsers();
    }

    @GetMapping("/{id}")
    public UserDTO getUserById(@PathVariable int id) throws UserNotFoundException {
        return userService.getUserById(id);
    }

    @PostMapping
    public UserDTO createNewUser(CreateUpdateUserDto createUpdateUserDto) throws InvalidUserDataException {
        return userService.createNewUser(createUpdateUserDto);
    }

    @PutMapping("/{id}")
    public UserDTO updateUserById(@PathVariable int id, @RequestBody CreateUpdateUserDto userToUpdate) throws UserNotFoundException, InvalidUserDataException {
        return userService.updateUserById(id, userToUpdate);
    }

    @DeleteMapping("/{id}")
    public UserDTO deleteUserById(@PathVariable int id) throws UserNotFoundException {
        return userService.deleteUserById(id);
    }
}
