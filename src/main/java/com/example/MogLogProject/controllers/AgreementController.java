package com.example.MogLogProject.controllers;

import com.example.MogLogProject.service.dto.AgreementDto;
import com.example.MogLogProject.service.dto.CreateUpdateAgreementDto;
import com.example.MogLogProject.service.exception.agreement.AgreementNotFoundException;
import com.example.MogLogProject.service.exception.agreement.InvalidAgreementDataException;
import com.example.MogLogProject.service.services.AgreementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/agreement")
public class AgreementController {

    @Autowired
    private AgreementService agreementService;

    @GetMapping
    public List<AgreementDto> getAllAgreements() { return agreementService.getAllAgreements() ; }

    @GetMapping("/{id}")
    public AgreementDto getAgreementById(@PathVariable int id)
            throws AgreementNotFoundException {
        return agreementService.getAgreementById(id);
    }

    @PostMapping
    public AgreementDto createNewAgreement(@RequestBody CreateUpdateAgreementDto createUpdateAgreementDto)
        throws InvalidAgreementDataException {
        return agreementService.createNewAgreement(createUpdateAgreementDto);
    }

    @PutMapping("/{id}")
    public AgreementDto updateAgreementById(@PathVariable int id, @RequestBody CreateUpdateAgreementDto createUpdateAgreementDto)
        throws InvalidAgreementDataException, AgreementNotFoundException {
        return agreementService.updateAgreementById(id, createUpdateAgreementDto);
    }

    @DeleteMapping("/{id}")
    public AgreementDto deleteAgreementById(@PathVariable int id) throws AgreementNotFoundException{
        return agreementService.deleteAgreementById(id);
    }

}
