package com.example.MogLogProject.controllers;

import com.example.MogLogProject.service.dto.CommentDto;
import com.example.MogLogProject.service.dto.CreateUpdateCommentDto;
import com.example.MogLogProject.service.exception.agreement.AgreementNotFoundException;
import com.example.MogLogProject.service.exception.comment.CommentNotFoundException;
import com.example.MogLogProject.service.exception.comment.InvalidCommentDataException;
import com.example.MogLogProject.service.exception.user.UserNotFoundException;
import com.example.MogLogProject.service.services.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/comment")
public class CommentController {

    @Autowired
    private CommentService commentService;

    @GetMapping
    public List<CommentDto> getAllComments(){
        return commentService.getAllComments();
    }

    @GetMapping("/{id}")
    public CommentDto getCommentById(@PathVariable int id) throws CommentNotFoundException {
        return commentService.getCommentById(id);
    }

    @PostMapping
    public CommentDto createNewComment(CreateUpdateCommentDto createUpdateCommentDto) throws InvalidCommentDataException, UserNotFoundException, AgreementNotFoundException {
        return commentService.createNewComment(createUpdateCommentDto);
    }

    @PutMapping("/{id}")
    public CommentDto updateCommentById(@PathVariable int id, CreateUpdateCommentDto createUpdateCommentDto)
        throws CommentNotFoundException, InvalidCommentDataException {
        return commentService.updateCommentById(id, createUpdateCommentDto);
    }

    @DeleteMapping("/{id}")
    public CommentDto deleteCommentById(@PathVariable int id) throws CommentNotFoundException{
        return deleteCommentById(id);
    }
}
